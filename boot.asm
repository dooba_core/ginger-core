; Ginger Core
; The core of the Ginger operating system

; DEBUG
str_test: db "Hello world foobar"
str_test_len: equ $-str_test

; Boot Sequence
boot_sequence:

	; ToDo: load L2 init?

	; DEBUG
	ld hl, str_test
	ld bc, str_test_len
	call syscall_debug_print
	dbg_loop_0:
		jp dbg_loop_0
