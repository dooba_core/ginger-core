; Ginger Core
; The core of the Ginger operating system

; Includes
include "inc/z80.asm"
include "inc/banyan.asm"
include "inc/ginger.asm"
include "inc/syscall/debug.asm"
include "inc/syscall/dmm.asm"
include "inc/syscall/vfs.asm"
include "inc/syscall/path.asm"
include "inc/syscall/task.asm"

; -----------------------------------------------------------------------------
; Absolute beginning of memory
; -----------------------------------------------------------------------------

; Origin
seek 0x0000
org 0x0000

; Enable Interrupt Mode 1
im 1

; Load Boot Stack
ld sp, (ctx_switch_sp)

; Ready for multi-tasking
ld a, 1
ld (ctx_switch_status), a

; Jump to boot sequence
jp boot_sequence

; -----------------------------------------------------------------------------
; Very early and low-level stuff - hand-placed to nicely fill the space
; right before the IM1 handler
; -----------------------------------------------------------------------------

; Relative Call Trampoline
include "rcall.asm"

; Syscall Routine
include "syscall.asm"

; -----------------------------------------------------------------------------
; This part needs to remain fixed at this location
; -----------------------------------------------------------------------------

; Context Switch Routine
include "ctx_switch.asm"

; -----------------------------------------------------------------------------
; From this point we can have everything float dynamically and just
; rely on the symbols file that will be generated
; -----------------------------------------------------------------------------

; String Utilities
include "str.asm"

; Syscalls
include "syscall/debug.asm"
include "syscall/dmm.asm"
include "syscall/vfs.asm"
include "syscall/path.asm"
include "syscall/task.asm"

; Boot Sequence
include "boot.asm"
