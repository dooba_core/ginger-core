; Ginger Core
; The core of the Ginger operating system

; Context Switch Routine (IM1 Interrupt Handler)
seek im1_int
org im1_int

	; Push context
	push af
	push bc
	push de
	push hl
	push ix
	push iy
	exx
	push bc
	push de
	push hl

	; Save SP to staging area
	ld (ctx_switch_sp), sp

	; Indicate to controller we are ready to switch context
	ld a, 1
	ld (ctx_switch_status), a

	; Wait for controller to switch context
	ctx_switch_wait:
		ld a, (ctx_switch_status)
		cp 0
		jp nz, ctx_switch_wait

	; Controller has replaced SP in staging area with the next task,
	; let's load it up, pop context and continue execution
	ld sp, (ctx_switch_sp)
	pop hl
	pop de
	pop bc
	exx
	pop iy
	pop ix
	pop hl
	pop de
	pop bc
	pop af
	ei
	ret
