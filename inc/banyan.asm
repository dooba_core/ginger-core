; Ginger Core
; The core of the Ginger operating system

; Banyan Interface Definitions

; -----------------------------------------------------------------------------
; Context Management
; -----------------------------------------------------------------------------

; Current Context Base
ctx_base: equ 0x0000

; Status
ctx_switch_status: equ 0xffeb

; Stack Pointer Staging Area
ctx_switch_sp: equ 0xffec

; -----------------------------------------------------------------------------
; Syscall Interface
; -----------------------------------------------------------------------------

; Interface Base Address
syscall_base: equ 0xffee

; Status
syscall_status_off: equ 0x00
syscall_status: equ syscall_base + syscall_status_off
syscall_status_wait: equ 0
syscall_status_done: equ 1
syscall_status_fail: equ 2
syscall_status_ack: equ 3

; Opcode
syscall_opcode_off: equ 0x01
syscall_opcode: equ syscall_base + syscall_opcode_off

; Arguments
syscall_args_off: equ 0x02
syscall_args: equ syscall_base + syscall_args_off

; Syscall Opcodes
syscall_opcode_debug: equ 0x00
syscall_opcode_dmm: equ 0x01
syscall_opcode_vfs: equ 0x02
syscall_opcode_path: equ 0x03
syscall_opcode_task: equ 0x04

; -----------------------------------------------------------------------------
; Task Signature Structure
; -----------------------------------------------------------------------------

; Task Signature
banyan_task_sig_l: equ 0x07a5
banyan_task_sig_h: equ 0xbaba

; Task Signature Macro
banyan_task_signature: macro
	dw banyan_task_sig_l
	dw banyan_task_sig_h
endm
