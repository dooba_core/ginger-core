; Ginger Core
; The core of the Ginger operating system

; Ginger Core Definitions

; -----------------------------------------------------------------------------
; Relative Call Infrastructure
; -----------------------------------------------------------------------------

; Relative Call Macro
rcall: macro call_target

	; Save registers
	exx

	; Load call target
	ld hl, call_target

	; Call Trampoline
	call rcall_trampoline
endm

; Relative Call Trampoline Address
rcall_trampoline: equ 0x000e

; -----------------------------------------------------------------------------
; Syscall Interface
; -----------------------------------------------------------------------------

; Syscall Routine Address
syscall: equ 0x001b
