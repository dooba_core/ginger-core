; Ginger Core
; The core of the Ginger operating system

; Banyan Syscall Interface - Debug

; Operation
syscall_debug_op: equ syscall_args + 0x00

; Print Arguments
syscall_debug_print_addr: equ syscall_args + 0x01
syscall_debug_print_size: equ syscall_args + 0x03

; Hex Dump Arguments
syscall_debug_hdump_addr: equ syscall_args + 0x01
syscall_debug_hdump_size: equ syscall_args + 0x03

; Operations
syscall_debug_op_print: equ 0x00
syscall_debug_op_hdump: equ 0x01
