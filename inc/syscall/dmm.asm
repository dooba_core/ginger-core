; Ginger Core
; The core of the Ginger operating system

; Banyan Syscall Interface - DMM

; Operation
syscall_dmm_op: equ syscall_args + 0x00

; Allocate Arguments
syscall_dmm_alloc_size: equ syscall_args + 0x01
syscall_dmm_alloc_addr: equ syscall_args + 0x03

; Free Arguments
syscall_dmm_free_addr: equ syscall_args + 0x01

; Accounting Arguments
syscall_dmm_accounting_info: equ syscall_args + 0x01

; Operations
syscall_dmm_op_alloc: equ 0x00
syscall_dmm_op_free: equ 0x01
syscall_dmm_op_accounting: equ 0x02

; Accounting Info Structure
syscall_dmm_accounting_info_available_size: equ 0x00
syscall_dmm_accounting_info_used_size: equ 0x02
syscall_dmm_accounting_info_available_regions: equ 0x04
syscall_dmm_accounting_info_used_regions: equ 0x06
syscall_dmm_accounting_info_available_slots: equ 0x08
syscall_dmm_accounting_info_used_slots: equ 0x0a
syscall_dmm_accounting_info_struct_size: equ 0x0c
