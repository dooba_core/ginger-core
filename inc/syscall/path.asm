; Ginger Core
; The core of the Ginger operating system

; Banyan Syscall Interface - Path

; Operation
syscall_path_op: equ syscall_args + 0x00

; Get Current Working Directory Arguments
syscall_path_getcwd_cwd: equ syscall_args + 0x01
syscall_path_getcwd_len: equ syscall_args + 0x03

; Change Working Directory Arguments
syscall_path_change_path: equ syscall_args + 0x01
syscall_path_change_len: equ syscall_args + 0x03

; Get Path List Arguments
syscall_path_getlist_addr: equ syscall_args + 0x01
syscall_path_getlist_len: equ syscall_args + 0x03

; Register Path Arguments
syscall_path_reg_path: equ syscall_args + 0x01
syscall_path_reg_len: equ syscall_args + 0x03

; Unregister Path Arguments
syscall_path_unreg_path: equ syscall_args + 0x01
syscall_path_unreg_len: equ syscall_args + 0x03

; Operations
syscall_path_op_getcwd: equ 0x00
syscall_path_op_change: equ 0x01
syscall_path_op_getlist: equ 0x02
syscall_path_op_reg: equ 0x03
syscall_path_op_unreg: equ 0x04

; Path List Size
syscall_path_list_size: equ 128

; Path List Delimiter (';')
syscall_path_list_delim: equ 0x3b
