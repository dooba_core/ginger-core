; Ginger Core
; The core of the Ginger operating system

; Banyan Syscall Interface - Task

; Operation
syscall_task_op: equ syscall_args + 0x00

; Kill Arguments
syscall_task_kill_id: equ syscall_args + 0x01

; Exec Arguments
syscall_task_exec_path: equ syscall_args + 0x01
syscall_task_exec_len: equ syscall_args + 0x03
syscall_task_exec_args: equ syscall_args + 0x04
syscall_task_exec_args_len: equ syscall_args + 0x06
syscall_task_exec_id: equ syscall_args + 0x07

; Spawn Arguments
syscall_task_spawn_addr: equ syscall_args + 0x01
syscall_task_spawn_size: equ syscall_args + 0x03
syscall_task_spawn_args: equ syscall_args + 0x05
syscall_task_spawn_args_len: equ syscall_args + 0x07
syscall_task_spawn_id: equ syscall_args + 0x09

; Output Arguments
syscall_task_output_data: equ syscall_args + 0x01
syscall_task_output_size: equ syscall_args + 0x03

; Get Next Arg Arguments
syscall_task_getnextarg_args: equ syscall_args + 0x01
syscall_task_getnextarg_args_len: equ syscall_args + 0x03
syscall_task_getnextarg_abuf: equ syscall_args + 0x04
syscall_task_getnextarg_alen: equ syscall_args + 0x06

; Operations
syscall_task_op_die: equ 0x00
syscall_task_op_kill: equ 0x01
syscall_task_op_exec: equ 0x02
syscall_task_op_spawn: equ 0x03
syscall_task_op_output: equ 0x04
syscall_task_op_getnextarg: equ 0x05

; Arguments Max Size
syscall_task_args_size: equ 64
