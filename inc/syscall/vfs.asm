; Ginger Core
; The core of the Ginger operating system

; Banyan Syscall Interface - VFS

; Operation
syscall_vfs_op: equ syscall_args + 0x00

; Open Arguments
syscall_vfs_open_handle: equ syscall_args + 0x01
syscall_vfs_open_path: equ syscall_args + 0x03
syscall_vfs_open_path_len: equ syscall_args + 0x05
syscall_vfs_open_flags: equ syscall_args + 0x06

; Close Arguments
syscall_vfs_close_handle: equ syscall_args + 0x01

; Read Arguments
syscall_vfs_read_handle: equ syscall_args + 0x01
syscall_vfs_read_data: equ syscall_args + 0x03
syscall_vfs_read_size: equ syscall_args + 0x05
syscall_vfs_read_rd: equ syscall_args + 0x07

; Write Arguments
syscall_vfs_write_handle: equ syscall_args + 0x01
syscall_vfs_write_data: equ syscall_args + 0x03
syscall_vfs_write_size: equ syscall_args + 0x05
syscall_vfs_write_wr: equ syscall_args + 0x07

; Read Directory Arguments
syscall_vfs_readdir_handle: equ syscall_args + 0x01
syscall_vfs_readdir_dirent: equ syscall_args + 0x03

; Create Object Arguments
syscall_vfs_mkobj_path: equ syscall_args + 0x01
syscall_vfs_mkobj_path_len: equ syscall_args + 0x03
syscall_vfs_mkobj_otype: equ syscall_args + 0x04

; Destroy Object Arguments
syscall_vfs_rmobj_path: equ syscall_args + 0x01
syscall_vfs_rmobj_path_len: equ syscall_args + 0x03

; Copy Object Arguments
syscall_vfs_cpobj_src: equ syscall_args + 0x01
syscall_vfs_cpobj_src_len: equ syscall_args + 0x03
syscall_vfs_cpobj_dst: equ syscall_args + 0x04
syscall_vfs_cpobj_dst_len: equ syscall_args + 0x06

; Move Object Arguments
syscall_vfs_mvobj_src: equ syscall_args + 0x01
syscall_vfs_mvobj_src_len: equ syscall_args + 0x03
syscall_vfs_mvobj_dst: equ syscall_args + 0x04
syscall_vfs_mvobj_dst_len: equ syscall_args + 0x06

; Operations
syscall_vfs_op_open: equ 0x00
syscall_vfs_op_close: equ 0x01
syscall_vfs_op_read: equ 0x02
syscall_vfs_op_write: equ 0x03
syscall_vfs_op_readdir: equ 0x04
syscall_vfs_op_mkobj: equ 0x05
syscall_vfs_op_rmobj: equ 0x06
syscall_vfs_op_cpobj: equ 0x07
syscall_vfs_op_mvobj: equ 0x08

; Path Max Length
syscall_vfs_path_maxlen: equ 64

; Path Element Max Length
syscall_vfs_path_element_maxlen: equ 24

; Handle Structure
syscall_vfs_handle_data: equ 0x00
syscall_vfs_handle_data_size: equ 32
syscall_vfs_handle_otype: equ syscall_vfs_handle_data_size
syscall_vfs_handle_size_0: equ syscall_vfs_handle_data_size + 0x01
syscall_vfs_handle_size_1: equ syscall_vfs_handle_data_size + 0x03
syscall_vfs_handle_size_2: equ syscall_vfs_handle_data_size + 0x05
syscall_vfs_handle_size_3: equ syscall_vfs_handle_data_size + 0x07
syscall_vfs_handle_pos_0: equ syscall_vfs_handle_data_size + 0x09
syscall_vfs_handle_pos_1: equ syscall_vfs_handle_data_size + 0x0b
syscall_vfs_handle_pos_2: equ syscall_vfs_handle_data_size + 0x0d
syscall_vfs_handle_pos_3: equ syscall_vfs_handle_data_size + 0x0f
syscall_vfs_handle_mp: equ syscall_vfs_handle_data_size + 0x11
syscall_vfs_handle_struct_size: equ syscall_vfs_handle_data_size + 0x13

; Directory Entry Structure
syscall_vfs_dirent_name: equ 0x00
syscall_vfs_dirent_name_len: equ syscall_vfs_path_element_maxlen
syscall_vfs_dirent_otype: equ syscall_vfs_path_element_maxlen + 0x01
syscall_vfs_dirent_size_0: equ syscall_vfs_path_element_maxlen + 0x02
syscall_vfs_dirent_size_1: equ syscall_vfs_path_element_maxlen + 0x04
syscall_vfs_dirent_size_2: equ syscall_vfs_path_element_maxlen + 0x06
syscall_vfs_dirent_size_3: equ syscall_vfs_path_element_maxlen + 0x08
syscall_vfs_dirent_struct_size: equ syscall_vfs_path_element_maxlen + 0x0a
