; Ginger Core
; The core of the Ginger operating system

; Relative Call Trampoline (HL = Call Target)
seek rcall_trampoline
org rcall_trampoline

	; Compute Real Address
	ld bc, (ctx_base)
	add hl, bc

	; Prepare for indirect call
	ld bc, rcall_return_addr
	push bc
	push hl

	; Restore registers
	exx

	; Perform indirect call
	ret

	; Return address
	rcall_return_addr:

	; Done
	ret
