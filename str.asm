; Ginger Core
; The core of the Ginger operating system

; Get String Length
; Arguments: HL = String pointer
; Returns: BC = String length
strlen:

	; Push registers
	push af

	; Save string pointer
	push hl

	; Scan string
	xor a
	ld bc, 0
	cpir

	; Compute length
	pop bc
	and a
	sbc hl, bc
	push hl
	pop bc

	; Pop registers
	pop af

	; Done
	ret
