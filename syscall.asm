; Ginger Core
; The core of the Ginger operating system

; Syscall Routine
; Output: A = Result
seek syscall
org syscall

	; Wait until syscall is available
	syscall_avail_loop:
		ld a, (syscall_status)
		cp syscall_status_wait
		jp nz, syscall_avail_loop

	; Loop until syscall is handled
	syscall_loop:

		; Trigger Syscall
		out (0x80), a

		; Check Status
		ld a, (syscall_status)
		cp syscall_status_wait
		jp z, syscall_loop

	; Done
	ret

; Acknowledge Syscall
syscall_ack:

	; Acknowledge Syscall Completion
	push af
	ld a, syscall_status_ack
	ld (syscall_status), a
	pop af

	; Done
	ret

; End of syscall routines
syscall_end:
