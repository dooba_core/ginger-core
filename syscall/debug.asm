; Ginger Core
; The core of the Ginger operating system

; Print
; Arguments: HL = String, BC = Length
; Output: A = Result
syscall_debug_print:

	; Setup syscall
	ld a, syscall_opcode_debug
	ld (syscall_opcode), a
	ld a, syscall_debug_op_print
	ld (syscall_debug_op), a
	ld (syscall_debug_print_addr), hl
	ld (syscall_debug_print_size), bc

	; Perform syscall
	call syscall

	; Ack syscall
	call syscall_ack

	; Done
	ret

; Hex Dump
; Arguments: HL = Data, BC = Size
; Output: A = Result
syscall_debug_hdump:

	; Push registers
	push af

	; Setup syscall
	ld a, syscall_opcode_debug
	ld (syscall_opcode), a
	ld a, syscall_debug_op_hdump
	ld (syscall_debug_op), a
	ld (syscall_debug_hdump_addr), hl
	ld (syscall_debug_hdump_size), bc

	; Perform syscall
	call syscall

	; Ack syscall
	call syscall_ack

	; Done
	ret
