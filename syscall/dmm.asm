; Ginger Core
; The core of the Ginger operating system

; Allocate
; Arguments: BC = Size
; Output: A = Result, HL = Address
syscall_dmm_alloc:

	; Setup syscall
	ld a, syscall_opcode_dmm
	ld (syscall_opcode), a
	ld a, syscall_dmm_op_alloc
	ld (syscall_dmm_op), a
	ld (syscall_dmm_alloc_size), bc

	; Perform syscall
	call syscall

	; Get Address
	ld hl, (syscall_dmm_alloc_addr)

	; Ack syscall
	call syscall_ack

	; Done
	ret

; Free
; Arguments: HL = Address
; Output: A = Result
syscall_dmm_free:

	; Setup syscall
	ld a, syscall_opcode_dmm
	ld (syscall_opcode), a
	ld a, syscall_dmm_op_free
	ld (syscall_dmm_op), a
	ld (syscall_dmm_alloc_addr), hl

	; Perform syscall
	call syscall

	; Ack syscall
	call syscall_ack

	; Done
	ret

; Accounting
; Arguments: HL = Accounting Info Address
; Output: A = Result
syscall_dmm_accounting:

	; Setup syscall
	ld a, syscall_opcode_dmm
	ld (syscall_opcode), a
	ld a, syscall_dmm_op_accounting
	ld (syscall_dmm_op), a
	ld (syscall_dmm_accounting_info), hl

	; Perform syscall
	call syscall

	; Ack syscall
	call syscall_ack

	; Done
	ret
