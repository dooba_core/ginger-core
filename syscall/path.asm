; Ginger Core
; The core of the Ginger operating system

; Get Current Working Directory
; Arguments: None
; Output: A = Result, HL = CWD, B = Length
syscall_path_getcwd:

	; Setup syscall
	ld a, syscall_opcode_path
	ld (syscall_opcode), a
	ld a, syscall_path_op_getcwd
	ld (syscall_path_op), a

	; Perform syscall
	call syscall

	; Get CWD & Length
	push af
	ld hl, (syscall_path_getcwd_cwd)
	ld a, (syscall_path_getcwd_len)
	ld b, a
	pop af

	; Ack syscall
	call syscall_ack

	; Done
	ret

; Change Working Directory
; Arguments: HL = Path, B = Length
; Output: A = Result
syscall_path_change:

	; Setup syscall
	ld a, syscall_opcode_path
	ld (syscall_opcode), a
	ld a, syscall_path_op_change
	ld (syscall_path_op), a
	ld (syscall_path_change_path), hl
	ld a, b
	ld (syscall_path_change_len), a

	; Perform syscall
	call syscall

	; Ack syscall
	call syscall_ack

	; Done
	ret

; Get Path List
; Arguments: HL = Buffer address (buffer must be at least 'syscall_path_list_size' bytes)
; Output: A = Result, B = List length
syscall_path_getlist:

	; Setup syscall
	ld a, syscall_opcode_path
	ld (syscall_opcode), a
	ld a, syscall_path_op_getlist
	ld (syscall_path_op), a
	ld (syscall_path_getlist_addr), hl

	; Perform syscall
	call syscall

	; Get Length
	push af
	ld a, (syscall_path_getlist_len)
	ld b, a
	pop af

	; Ack syscall
	call syscall_ack

	; Done
	ret

; Register Path into List
; Arguments: HL = Path, B = Length
; Output: A = Result
syscall_path_reg:

	; Setup syscall
	ld a, syscall_opcode_path
	ld (syscall_opcode), a
	ld a, syscall_path_op_reg
	ld (syscall_path_op), a
	ld (syscall_path_reg_path), hl
	ld a, b
	ld (syscall_path_reg_len), a

	; Perform syscall
	call syscall

	; Ack syscall
	call syscall_ack

	; Done
	ret

; Unregister Path from List
; Arguments: HL = Path, B = Length
; Output: A = Result
syscall_path_unreg:

	; Setup syscall
	ld a, syscall_opcode_path
	ld (syscall_opcode), a
	ld a, syscall_path_op_unreg
	ld (syscall_path_op), a
	ld (syscall_path_unreg_path), hl
	ld a, b
	ld (syscall_path_unreg_len), a

	; Perform syscall
	call syscall

	; Ack syscall
	call syscall_ack

	; Done
	ret
