; Ginger Core
; The core of the Ginger operating system

; Die
; Arguments: None
; Output: A = Result
syscall_task_die:

	; Setup syscall
	ld a, syscall_opcode_task
	ld (syscall_opcode), a
	ld a, syscall_task_op_die
	ld (syscall_task_op), a

	; Perform syscall
	call syscall

	; Ack syscall
	call syscall_ack

	; Loop
	syscall_task_die_loop:
	jp syscall_task_die_loop

; Kill
; Arguments: DE = Task ID
; Output: A = Result
syscall_task_kill:

	; Setup syscall
	ld a, syscall_opcode_task
	ld (syscall_opcode), a
	ld a, syscall_task_op_kill
	ld (syscall_task_op), a
	ld (syscall_task_kill_id), de

	; Perform syscall
	call syscall

	; Ack syscall
	call syscall_ack

	; Done
	ret

; Execute from VFS
; Arguments: HL = Path, B = Length, DE = Args, A = Args length
; Output: A = Result, DE = Task ID
syscall_task_exec:

	; Setup syscall
	push af
	ld a, syscall_opcode_task
	ld (syscall_opcode), a
	ld a, syscall_task_op_exec
	ld (syscall_task_op), a
	ld (syscall_task_exec_path), hl
	ld a, b
	ld (syscall_task_exec_len), a
	ld (syscall_task_exec_args), de
	pop af
	ld (syscall_task_exec_args_len), a

	; Perform syscall
	call syscall

	; Get Task ID
	ld de, (syscall_task_exec_id)

	; Ack syscall
	call syscall_ack

	; Done
	ret

; Spawn
; Arguments: HL = Data address, BC = Size, DE = Args, A = Args length
; Output: A = Result, DE = Task ID
syscall_task_spawn:

	; Setup syscall
	push af
	ld a, syscall_opcode_task
	ld (syscall_opcode), a
	ld a, syscall_task_op_spawn
	ld (syscall_task_op), a
	ld (syscall_task_spawn_addr), hl
	ld (syscall_task_spawn_size), bc
	ld (syscall_task_spawn_args), de
	pop af
	ld (syscall_task_spawn_args_len), a

	; Perform syscall
	call syscall

	; Get Task ID
	ld de, (syscall_task_spawn_id)

	; Ack syscall
	call syscall_ack

	; Done
	ret

; Output
; Arguments: HL = Data, BC = Size
; Output: A = Result
syscall_task_output:

	; Setup syscall
	ld a, syscall_opcode_task
	ld (syscall_opcode), a
	ld a, syscall_task_op_output
	ld (syscall_task_op), a
	ld (syscall_task_output_data), hl
	ld (syscall_task_output_size), bc

	; Perform syscall
	call syscall

	; Ack syscall
	call syscall_ack

	; Done
	ret

; Get Next Argument
; Arguments: HL = Args, B = Args length, DE = Arg buffer
; Output: A = Result, HL = Args, B = Args length, C = Resulting argument length
syscall_task_getnextarg:

	; Setup syscall
	ld a, syscall_opcode_task
	ld (syscall_opcode), a
	ld a, syscall_task_op_getnextarg
	ld (syscall_task_op), a
	ld (syscall_task_getnextarg_args), hl
	ld a, b
	ld (syscall_task_getnextarg_args_len), a
	ld (syscall_task_getnextarg_abuf), de

	; Perform syscall
	call syscall

	; Get updated args & resulting arg length
	push af
	ld hl, (syscall_task_getnextarg_args)
	ld a, (syscall_task_getnextarg_args_len)
	ld b, a
	ld a, (syscall_task_getnextarg_alen)
	ld c, a
	pop af

	; Ack syscall
	call syscall_ack

	; Done
	ret
