; Ginger Core
; The core of the Ginger operating system

; Open
; Arguments: DE = Handle, HL = Path, B = Path length, C = Flags
; Output: A = Result
syscall_vfs_open:

	; Setup syscall
	ld a, syscall_opcode_vfs
	ld (syscall_opcode), a
	ld a, syscall_vfs_op_open
	ld (syscall_vfs_op), a
	ld (syscall_vfs_open_handle), de
	ld (syscall_vfs_open_path), hl
	ld a, b
	ld (syscall_vfs_open_path_len), a
	ld a, c
	ld (syscall_vfs_open_flags), a

	; Perform syscall
	call syscall

	; Ack syscall
	call syscall_ack

	; Done
	ret

; Close
; Arguments: DE = Handle
; Output: A = Result
syscall_vfs_close:

	; Setup syscall
	ld a, syscall_opcode_vfs
	ld (syscall_opcode), a
	ld a, syscall_vfs_op_close
	ld (syscall_vfs_op), a
	ld (syscall_vfs_close_handle), de

	; Perform syscall
	call syscall

	; Ack syscall
	call syscall_ack

	; Done
	ret

; Read
; Arguments: DE = Handle, HL = Data, BC = Size
; Output: A = Result, BC = Resulting size
syscall_vfs_read:

	; Setup syscall
	ld a, syscall_opcode_vfs
	ld (syscall_opcode), a
	ld a, syscall_vfs_op_read
	ld (syscall_vfs_op), a
	ld (syscall_vfs_read_handle), de
	ld (syscall_vfs_read_data), hl
	ld (syscall_vfs_read_size), bc

	; Perform syscall
	call syscall

	; Get resulting size
	ld bc, (syscall_vfs_read_rd)

	; Ack syscall
	call syscall_ack

	; Done
	ret

; Write
; Arguments: DE = Handle, HL = Data, BC = Size
; Output: A = Result, BC = Resulting size
syscall_vfs_write:

	; Setup syscall
	ld a, syscall_opcode_vfs
	ld (syscall_opcode), a
	ld a, syscall_vfs_op_write
	ld (syscall_vfs_op), a
	ld (syscall_vfs_write_handle), de
	ld (syscall_vfs_write_data), hl
	ld (syscall_vfs_write_size), bc

	; Perform syscall
	call syscall

	; Get resulting size
	ld bc, (syscall_vfs_write_wr)

	; Ack syscall
	call syscall_ack

	; Done
	ret

; Read Directory
; Arguments: DE = Handle, HL = Directory entry
; Output: A = Result
syscall_vfs_readdir:

	; Setup syscall
	ld a, syscall_opcode_vfs
	ld (syscall_opcode), a
	ld a, syscall_vfs_op_readdir
	ld (syscall_vfs_op), a
	ld (syscall_vfs_readdir_handle), de
	ld (syscall_vfs_readdir_dirent), hl

	; Perform syscall
	call syscall

	; Ack syscall
	call syscall_ack

	; Done
	ret

; Create Object
; Arguments: HL = Path, B = Path length, C = Object type
; Output: A = Result
syscall_vfs_mkobj:

	; Setup syscall
	ld a, syscall_opcode_vfs
	ld (syscall_opcode), a
	ld a, syscall_vfs_op_mkobj
	ld (syscall_vfs_op), a
	ld (syscall_vfs_mkobj_path), hl
	ld a, b
	ld (syscall_vfs_mkobj_path_len), a
	ld a, c
	ld (syscall_vfs_mkobj_otype), a

	; Perform syscall
	call syscall

	; Ack syscall
	call syscall_ack

	; Done
	ret

; Destroy Object
; Arguments: HL = Path, B = Path length
; Output: A = Result
syscall_vfs_rmobj:

	; Setup syscall
	ld a, syscall_opcode_vfs
	ld (syscall_opcode), a
	ld a, syscall_vfs_op_rmobj
	ld (syscall_vfs_op), a
	ld (syscall_vfs_rmobj_path), hl
	ld a, b
	ld (syscall_vfs_rmobj_path_len), a

	; Perform syscall
	call syscall

	; Ack syscall
	call syscall_ack

	; Done
	ret

; Copy Object
; Arguments: HL = Source path, B = Source path length, DE = Destination path, C = Destination path length
; Output: A = Result
syscall_vfs_cpobj:

	; Setup syscall
	ld a, syscall_opcode_vfs
	ld (syscall_opcode), a
	ld a, syscall_vfs_op_cpobj
	ld (syscall_vfs_op), a
	ld (syscall_vfs_cpobj_src), hl
	ld a, b
	ld (syscall_vfs_cpobj_src_len), a
	ld (syscall_vfs_cpobj_dst), de
	ld a, c
	ld (syscall_vfs_cpobj_dst_len), a

	; Perform syscall
	call syscall

	; Ack syscall
	call syscall_ack

	; Done
	ret

; Move Object
; Arguments: HL = Source path, B = Source path length, DE = Destination path, C = Destination path length
; Output: A = Result
syscall_vfs_mvobj:

	; Setup syscall
	ld a, syscall_opcode_vfs
	ld (syscall_opcode), a
	ld a, syscall_vfs_op_mvobj
	ld (syscall_vfs_op), a
	ld (syscall_vfs_mvobj_src), hl
	ld a, b
	ld (syscall_vfs_mvobj_src_len), a
	ld (syscall_vfs_mvobj_dst), de
	ld a, c
	ld (syscall_vfs_mvobj_dst_len), a

	; Perform syscall
	call syscall

	; Ack syscall
	call syscall_ack

	; Done
	ret
